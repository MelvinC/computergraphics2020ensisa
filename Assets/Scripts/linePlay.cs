﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class linePlay : MonoBehaviour
{
    private bool isPlaying;

    public void PlayOrStop()
    {
        isPlaying = !isPlaying;
        if (isPlaying)
        {
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }
        GetComponent<Animator>().Play("LineAnim");
    }

    // Start is called before the first frame update
    void Start()
    {
        isPlaying = false;
        gameObject.SetActive(false);


    }

    // Update is called once per frame
    public void Update()
    {
        if (isPlaying)
        {
            
            GetComponent<Animator>().speed = 1;
        }
        else
        {
            GetComponent<Animator>().speed = 0;
        }
    }
}
