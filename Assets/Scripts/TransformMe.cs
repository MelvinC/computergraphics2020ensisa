﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformMe : MonoBehaviour
{

    private RotateMe cylinder;
    public float Q;


    public void Slider_Change(float f)
    {
        Q = f;
    }
    // Start is called before the first frame update
    void Start()
    {
        GameObject go = GameObject.Find("FirstCylinder");
        cylinder = (RotateMe)go.GetComponent(typeof(RotateMe));
    }

    // Update is called once per frame
    void Update()
    {
        cylinder.Slider_Change(Q);
    }
}
