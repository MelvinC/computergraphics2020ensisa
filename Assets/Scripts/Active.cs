﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Active : MonoBehaviour
{
    private bool isActive;
    public void Activate()
    {
        isActive = !isActive;
        if (isActive)
            gameObject.SetActive(true);
        else
            gameObject.SetActive(false);
    }
    // Start is called before the first frame update
    void Start()
    {
        isActive = true;
        gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
