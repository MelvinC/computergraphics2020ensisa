﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Desactive : MonoBehaviour
{
    private bool isActive;
    public void Activate()
    {
        isActive = !isActive;
        if (isActive)
            gameObject.SetActive(true);
        else
            gameObject.SetActive(false);
    }
    // Start is called before the first frame update
    void Start()
    {
        isActive = false;
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }
}